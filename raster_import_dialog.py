# -*- coding: utf-8 -*-
"""
/***************************************************************************
 raster2postgis
 QGIS plugin to import raster files to a postgis database
                              -------------------
        begin                : 2019-09-20
        copyright            : (C) 2019 by Geomaster
        email                : geral@geomaster.pt
        git sha              : $Format:%H$
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License version 2 as     *
 *   published by the Free Software Foundation.                            *
 *                                                                         *
 ***************************************************************************/
"""
import os
import gdal
import re
import math

from PyQt5 import uic
from PyQt5.QtWidgets import QApplication, QDialog, QFileDialog, QDialogButtonBox
from PyQt5.QtCore import Qt, pyqtSlot, QThread, pyqtSignal
from PyQt5.QtGui import QCursor

from qgis.core import QgsCoordinateReferenceSystem

from . import qgis_configs
from .upload_raster import PostgisUploader

FORM_CLASS, _ = uic.loadUiType(os.path.join(
    os.path.dirname(__file__), 'ui/raster_import_dialog.ui'))


class RasterImportDialog(QDialog, FORM_CLASS):
    def __init__(self, parent=None):
        super(RasterImportDialog, self).__init__(parent)

        self.setupUi(self)

        self.widget.setHidden(True)
        self.buttonBox.button(
            QDialogButtonBox.Apply).clicked.connect(self.process)

    def showEvent(self, event):
        super(RasterImportDialog, self).showEvent(event)

        self.importProcess = None
        self.uploadList = []
        self.fillDataSources()

    def fillDataSources(self):
        self.connCombo.clear()
        dblist = qgis_configs.listDataSources()
        self.connCombo.addItems(dblist.keys())

    def getConnection(self):
        return self.connCombo.currentText()

    def urlify(self, s):
        # Remove all non-word characters (everything except numbers and letters)
        s = re.sub(r"[^\w\s]", '', s)

        # Replace all runs of whitespace with a single dash
        s = re.sub(r"\s+", '-', s)

        return s

    def getMetadata(self, filename):
        ds = gdal.Open(filename)
        # pattern that searches for EPSG values
        pattern = re.compile(r'\[?EPSG[^\]]*\]')
        try:
            # captures every EPSG in the projection description (contains elipsoid, units, etc)
            epsglist = pattern.findall(ds.GetProjection())
            valid = False
            if (len(epsglist) > 0):
                epsg = epsglist[-1]
                pattern = re.compile(r'[\'"]\d*[\'"]')
                epsg = pattern.search(epsg)
                if epsg is not None:
                    ssrid = epsg.group().strip('\"\'')
                    pj = QgsCoordinateReferenceSystem(ssrid)
                    if pj.isValid():
                        self.sridName.setCrs(pj)
                        self.pSridName.setCrs(pj)
                        valid = True

            if valid is not True:
                self.plainTextEdit.appendPlainText(
                    'Warning: Unable to find epsg.')

            # get aux files
            self.uploadList = ds.GetFileList()

            # suggest table name
            tname = os.path.splitext(os.path.basename(filename))[0]
            tname = self.urlify(tname)
            tname = 'r2pg_' + tname if tname[0].isdigit() else tname
            re.sub(r'[^$]', '', tname)
            tname = tname[3:] if tname.startswith('pg_') else tname
            tname = tname[:63] if len(tname) > 63 else tname
            self.outTableName.setText(tname.lower())
        except Exception:
            self.rasterFilePath.clear()
            self.plainTextEdit.appendPlainText('Error: Invalid file type')
        finally:
            del ds

    @pyqtSlot('PyQt_PyObject', name='writeText')
    def writeText(self, text):
        self.plainTextEdit.appendPlainText(text)

    def process(self):
        fileName = str(self.rasterFilePath.filePath())
        outSchema = str(self.outSchemaName.text())
        outTable = str(self.outTableName.text())

        # validate epsg
        if not self.sridName.crs().authid() or len(self.sridName.crs().authid().split(':')) < 2:
            self.plainTextEdit.appendPlainText('Error: Invalid source srid')
            return
        if not self.pSridName.crs().authid() or len(self.pSridName.crs().authid().split(':')) < 2:
            self.plainTextEdit.appendPlainText(
                'Error: Invalid projection srid')
            return

        conString = qgis_configs.getConnString(self, self.getConnection())

        epsg = self.sridName.crs().authid().split(':')[1]
        pepsg = self.pSridName.crs().authid().split(':')[1]
        nOver = self.nOverviews.value()
        blockSizeX = str(self.blckSizeX.value())
        blockSizeY = str(self.blckSizeY.value())
        isExternal = self.fsRasterCheckBox.isChecked()

        self.importProcess = rasterImportProcess(conString, fileName, outSchema, outTable, epsg,
                                                 pepsg, blockSizeX, blockSizeY, nOver, isExternal,
                                                 self.uploadList, self.setupDBCheckBox.isChecked())

        self.importProcess.signal.connect(self.writeText)
        self.importProcess.finished.connect(self.finishedImportRaster)

        QApplication.setOverrideCursor(QCursor(Qt.WaitCursor))
        self.importProcess.start()

    def finishedImportRaster(self):
        self.plainTextEdit.appendPlainText(
            "Finished import process.\n*******\n")
        QApplication.restoreOverrideCursor()

    def reject(self):
        if self.importProcess is not None and self.importProcess.isRunning() and self.importProcess.importer is not None:
            self.importProcess.importer.cancel()
        else:
            self.plainTextEdit.clear()
            super(RasterImportDialog, self).reject()


class rasterImportProcess(QThread):
    signal = pyqtSignal('PyQt_PyObject')

    def __init__(self, conString, fileName, outSchema, outTable, epsg, pepsg, blockSizeX, blockSizeY, nOver, isExternal, uploadList, setupDB):
        QThread.__init__(self)

        self.conString = conString
        self.outSchema = outSchema
        self.outTable = outTable

        self.file = fileName
        self.uploadList = uploadList
        self.isExternal = isExternal

        self.epsg = epsg
        self.pepsg = pepsg
        self.tileSize = blockSizeX+"x"+blockSizeY
        self.overviews = ','.join(
            list(map(lambda x: f"{math.pow(2, x):.0f}", range(1, nOver+1))))

        self.importer = None
        self.setupDB = setupDB

    def write(self, text):
        self.signal.emit(text)

    def run(self):
        try:
            # Start uploading session
            self.write("Connecting to database...")
            remotePath = '' if self.isExternal else '/tmp/'
            self.importer = PostgisUploader(self.file, self.conString, self.outSchema, self.outTable, self.epsg,
                                            self.pepsg, self.tileSize, self.overviews, remotePath, self.isExternal, self.uploadList)
            self.write("Connection successful")

            # Check if setup DB
            if self.setupDB:
                self.write("Setting up database")
                self.importer.setupDB()
                self.write('Finished setup')

            # Store raster file
            self.write("Uploading raster files")
            self.importer.storeRaster()
            self.write('Finished upload')

            # call raster2pgsql
            self.write('Setting up raster in postgis')
            self.importer.raster2pgsql()
            self.write('Finished importing raster')
        except Exception as e:
            self.write("Failed.")
            self.write(("Exception: {}".format(e)))
            if self.importer:
                self.write("Applying rollbacks")
                self.importer.doRollback()
        finally:
            if self.importer:
                self.write("Closing uploading session.")
                self.importer.exit()
