# -*- coding: utf-8 -*-
"""
/***************************************************************************
 raster2postgis
 QGIS plugin to import raster files to a postgis database
                              -------------------
        begin                : 2019-09-20
        copyright            : (C) 2019 by Geomaster
        email                : geral@geomaster.pt
        git sha              : $Format:%H$
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License version 2 as     *
 *   published by the Free Software Foundation.                            *
 *                                                                         *
 ***************************************************************************/
"""
import psycopg2
from psycopg2 import sql
from psycopg2.extensions import lobject

import os


def read_in_chunks(file_object, chunk_size=1024):
    """Lazy function (generator) to read a file piece by piece.
    Default chunk size: 1k."""
    while True:
        data = file_object.read(chunk_size)
        if not data:
            break
        yield data


class PostgisUploader:
    def __init__(self, file_path, conString, outSchema, outTable, epsg, pepsg, tileSize, overviews, remotePath, isExternal, uploadList):
        self.outSchema = outSchema
        self.outTable = outTable
        self.logTable = 'r2pg_log'

        self.file = file_path
        self.uploadList = uploadList
        self.isExternal = isExternal

        self.lobjects = []
        self.remotePath = remotePath
        self.rfile = None
        self.uid = None

        self.epsg = epsg
        self.pepsg = pepsg
        self.tileSize = tileSize
        self.overviews = overviews

        self.conn = psycopg2.connect(conString)
        self.cur = self.conn.cursor()

        self.callFunction = False
        self.checkContext()

    def checkContext(self):
        # check if superuser
        try:
            self.cur.execute(
                "select usesuper from pg_user where usename = CURRENT_USER;")
            isSuper = self.cur.fetchone()[0]
        except Exception as e:
            self.conn.rollback()
            raise(e)

        # check if functions exist
        procFuncs = True
        try:
            self.cur.execute("select 'public.r2pg_proc_func'::regproc;")
            self.cur.execute("select 'public.r2pg_stor_func'::regproc;")
        except Exception:
            self.conn.rollback()
            procFuncs = False

        # check schema
        schema = False
        try:
            self.cur.execute(
                "select exists(select 1 from pg_namespace where nspname = %s)", (self.outSchema,))
            if self.cur.fetchone()[0] is True:
                schema = True
        except Exception as e:
            self.conn.rollback()
            raise(e)

        # check postgis
        postgis = False
        try:
            self.cur.execute(
                "select exists(select 1 from pg_extension where extname = 'postgis')")
            if self.cur.fetchone()[0] is True:
                postgis = True
        except Exception as e:
            self.conn.rollback()
            raise(e)

        # setup context if necessary
        if isSuper and (not procFuncs or not schema or not postgis):
            self.setupDB()

        # throw exception if not superuser and faulty context
        if not isSuper and (not procFuncs or not schema or not postgis):
            msgf = 'Unable to run process. ' if not procFuncs else ''
            msgs = 'Unable to create schema. ' if not schema else ''
            msgp = 'Unable to setup postgis. ' if not schema else ''
            raise ValueError(
                msgs + msgf + msgp + 'Please use a connection as superuser first for setup')

        if not isSuper and (procFuncs and schema):
            self.callFunction = True

    def setupDB(self):
        pec = "create extension if not exists postgis"
        csc = "create schema if not exists {}"
        sfc = (
            "create or replace function r2pg_stor_func(integer, text, boolean) returns boolean as $r2pg_stor_func$\n"
            "    declare\n"
            "        loid integer := $1;\n"
            "        fpath text := $2;\n"
            "        persist boolean := $3;\n"
            "        dpath text;\n"
            "    begin\n"
            "        if persist is TRUE then\n"
            "            dpath := (select setting from pg_settings where name = 'data_directory');\n"
            "            execute format('select lo_export(%1$s::oid, %2$L)', loid, dpath || fpath);\n"
            "            return TRUE;\n"
            "        else\n"
            "            execute format('select lo_export(%1$s::oid, %2$L)', loid, fpath);\n"
            "            return TRUE;\n"
            "        end if;\n"
            "    end;\n"
            "$r2pg_stor_func$ language plpgsql security definer;"
        )
        pfc = (
            "create or replace function r2pg_proc_func(text, text, text, text, text, text, text, text, text, text, text, boolean) returns boolean as $r2pg_stor_func$\n"
            "    declare\n"
            "        outSchema text := $1;\n"
            "        logTable text := $2;\n"
            "        outTable text := $3;\n"
            "        epsg text := $4;\n"
            "        pepsg text := $5;\n"
            "        rfile text := $6;\n"
            "        gdalWarpOut text := $7;\n"
            "        tileSize text := $8;\n"
            "        overviews text := $9;\n"
            "        extFlag text := $10;\n"
            "        dbname text := $11;\n"
            "        persist boolean := $12;\n"
            "        dpath text;\n"
            "    begin\n"
            "        if persist is TRUE then\n"
            "            dpath := (select setting from pg_settings where name = 'data_directory');\n"
            "        else\n"
            "            dpath := '';\n"
            "        end if;\n"
            "        execute format('create table if not exists %1$s.%2$s(log varchar)', outSchema, logTable);\n"
            "        execute format('truncate table %1$s.%2$s', outSchema, logTable);\n"
            "        execute format('COPY %1$s.%2$s FROM PROGRAM '' gdalwarp -s_srs EPSG:%3$s -t_srs EPSG:%4$s -of vrt ''%5$L'' /vsistdout/"
            " | gdal_translate -co compress=lzw -co PREDICTOR=2 /vsistdin/ ''%6$L''''', outSchema, logTable, epsg, pepsg, dpath || rfile, dpath || gdalWarpOut);\n"
            "        execute format('COPY %1$s.%2$s FROM PROGRAM '' raster2pgsql -s %3$s -d -I -C -F -M -t %4$s -l %5$s %6$s ''%7$L'' %8$s.%9$s"
            " | psql %10$s''', outSchema, logTable, pepsg, tileSize, overviews, extFlag, dpath || gdalWarpOut, outSchema, outTable, dbname);\n"
            "        return TRUE;\n"
            "    end;\n"
            "$r2pg_stor_func$ language plpgsql security definer;"
        )
        self.cur.execute(pec)
        self.cur.execute(sql.SQL(csc).format(sql.Identifier(self.outSchema)))
        self.cur.execute(sfc)
        self.cur.execute(pfc)
        self.conn.commit()

    def storeRaster(self):
        if not self.callFunction:
            if self.isExternal:
                self.cur.execute("show data_directory;")
                self.remotePath = self.cur.fetchone()[0] + '/'

        for f in self.uploadList:
            with open(f, 'rb') as fd:
                # Initate the session with postgresql to write large object instance
                lobj = lobject(self.conn, 0, 'w')
                # Write the data to database
                for piece in read_in_chunks(fd, 1024000):
                    lobj.write(piece)
                # Commit transaction
                self.conn.commit()
                self.lobjects.append(lobj.oid)

                # get uid for remote file names
                if self.uid is None:
                    self.uid = str(lobj.oid)
                rfile = self.remotePath + self.uid + \
                    '_' + os.path.basename(f)
                # keep reference to main raster file
                if os.path.basename(f) == os.path.basename(self.file):
                    self.rfile = rfile

                # call export
                if not self.callFunction:
                    self.cur.execute(
                        "select lo_export(%s::oid, %s)", (lobj.oid, rfile))
                else:
                    self.cur.execute(
                        "select r2pg_stor_func(%s, %s, %s)", (lobj.oid, rfile, self.isExternal))

    def raster2pgsql(self):
        # TODO: check if table already exists
        # TODO: check if connection as postgres was possible, if not ask for credentials

        gdalWarpOut = os.path.splitext(self.rfile)[0] + '_p.tif'
        extFlag = '-R' if self.isExternal else ''

        if not self.callFunction:
            self.cur.execute(sql.SQL("create table if not exists {}.{}(log varchar)").format(
                sql.Identifier(self.outSchema), sql.Identifier(self.logTable)))
            self.cur.execute(sql.SQL("truncate table {}.{}").format(
                sql.Identifier(self.outSchema), sql.Identifier(self.logTable)))
            self.conn.commit()

            # call gdalwarp
            cmdGd = sql.SQL(
                "COPY {}.{} FROM PROGRAM ' gdalwarp -s_srs EPSG:" +
                self.epsg + " -t_srs EPSG:" + self.pepsg + " -of vrt ''" + self.rfile
                + "'' /vsistdout/ | gdal_translate -co compress=lzw -co PREDICTOR=2 /vsistdin/ ''" + gdalWarpOut + "'''").format(
                sql.Identifier(self.outSchema), sql.Identifier(self.logTable))
            # print(cmdGd)
            self.cur.execute(cmdGd)
            try:
                if int(self.cur.statusmessage.split()[-1]) == 0:
                    raise ValueError(
                        'Problem processing raster on the database')
            except Exception as e:
                raise (e)

            # call raster2pgsql
            cmd = sql.SQL(
                "COPY {}.{} FROM PROGRAM ' raster2pgsql -s " +
                self.pepsg + " -d -I -C -F -M -t " + self.tileSize
                + " -l " + self.overviews + ' ' + extFlag + " ''" + gdalWarpOut + "'' {}.{} | psql ''" +
                self.conn.get_dsn_parameters()['dbname'] + "'''").format(
                    sql.Identifier(self.outSchema), sql.Identifier(self.logTable), sql.Identifier(self.outSchema), sql.Identifier(self.outTable))
            # print(cmd)
            self.cur.execute(cmd)
            try:
                if int(self.cur.statusmessage.split()[-1]) == 0:
                    raise ValueError(
                        'Problem importing raster on the database')
            except Exception as e:
                raise (e)
        else:
            self.cur.execute("select r2pg_proc_func(%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)",
                             (self.outSchema, self.logTable, self.outTable, self.epsg, self.pepsg, self.rfile, gdalWarpOut,
                              self.tileSize, self.overviews, extFlag, self.conn.get_dsn_parameters()['dbname'], self.isExternal))
            try:
                if not self.cur.fetchone()[0] == True:
                    raise ValueError(
                        'Problem importing raster on the database')
            except Exception as e:
                raise (e)

    def doRollback(self):
        if self.conn:
            self.conn.rollback()

    def cancel(self):
        if self.conn:
            self.conn.cancel()

    def exit(self):
        for oid in self.lobjects:
            self.cur.execute("select lo_unlink(%s::oid)", (oid,))

        self.conn.commit()
        self.cur.close()
        self.conn.close()
