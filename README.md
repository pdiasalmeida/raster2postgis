# raster2postgis
Allows you to import raster files to a postgis database.

This plugin acts as a front-end for postgis's raster2pgsql utility, requiring said utility to be installed in the database server used.

In order to sucessfully import a raster using this plugin the connection has to be configured with a user with role superuser at least once to setup execution environment.

## Developement
The following steps can be used to setup the project for testing and developing:

### Dependencies
The following software is needed to develop and build the puglin
```
apt install qttools5-dev-tools
```

This plugin __requires postgis and gdal__ to be installed in the database server in order to import a raster
```
apt install postgis
apt install gdal
```

### Virtualenv
Setup your pyhon environment
```
apt install python-virtualenv

virtualenv -p python3 env
source env/bin/activate
pip3 install psycopg2
pip3 install pyqt5
```

### Edit Qt dialogs
Use Qt designer to edit the .ui files
```
designer -qt5 "<path to .ui file>"
```

### Preview Qt dialogs
You can run the following command in order to preview ui dialogs
```
pyuic5 -d -p "<path to .ui file>"
```

### Deploy
Deploy and test in your local QGIS 3 installation
```
make deploy
```
Open plugin manager in QGIS, then search for this plugin and enable it.
